module gitlab.com/bearsh/wshandler

go 1.15

require (
	github.com/sirupsen/logrus v1.7.0
	nhooyr.io/websocket v1.8.6
)
