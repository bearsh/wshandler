package wshandler

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"nhooyr.io/websocket"
)

// WsHandler enables broadcasting to a set of subscribers.
type WsHandler struct {
	subscribersMu   sync.Mutex
	subscribers     map[*Subscriber]struct{}
	OnNewSubscriber func(s *Subscriber)
	AcceptOptions   *websocket.AcceptOptions
}

// NewWsHandler constructs a WsHandler with the defaults.
func NewWsHandler() *WsHandler {
	wsh := &WsHandler{
		subscribers: make(map[*Subscriber]struct{}),
	}
	return wsh
}

// Subscriber represents a subscriber.
// Messages are sent on the msgs channel and if the client
// cannot keep up with the messages, closeSlow is called.
type Subscriber struct {
	msgs      chan []byte
	closeSlow func()
}

// SubscribeHandler accepts the WebSocket connection and then subscribes
// it to all future messages.
func (ws *WsHandler) SubscribeHandler(w http.ResponseWriter, r *http.Request) {
	c, err := websocket.Accept(w, r, ws.AcceptOptions)
	if err != nil {
		log.Errorf("%v", err)
		return
	}
	defer c.Close(websocket.StatusInternalError, "")

	err = ws.subscribe(r.Context(), c)
	if errors.Is(err, context.Canceled) {
		return
	}
	if websocket.CloseStatus(err) == websocket.StatusNormalClosure ||
		websocket.CloseStatus(err) == websocket.StatusGoingAway {
		return
	}
	if err != nil {
		log.Errorf("%v", err)
		return
	}
}

// EnableDebugMode set the AcceptOptions.InsecureSkipVerify = true
func (ws *WsHandler) EnableDebugMode() {
	if ws.AcceptOptions == nil {
		ws.AcceptOptions = &websocket.AcceptOptions{InsecureSkipVerify: true}
	} else {
		ws.AcceptOptions.InsecureSkipVerify = true
	}
}

// subscribe subscribes the given WebSocket to all broadcast messages.
// It creates a subscriber with a buffered msgs chan to give some room to slower
// connections and then registers the subscriber. It then listens for all messages
// and writes them to the WebSocket. If the context is cancelled or
// an error occurs, it returns and deletes the subscription.
//
// It uses CloseRead to keep reading from the connection to process control
// messages and cancel the context if the connection drops.
func (ws *WsHandler) subscribe(ctx context.Context, c *websocket.Conn) error {
	ctx = c.CloseRead(ctx)

	s := &Subscriber{
		msgs: make(chan []byte, 10),
		closeSlow: func() {
			c.Close(websocket.StatusPolicyViolation, "connection too slow to keep up with messages")
		},
	}
	ws.addSubscriber(s)
	defer ws.deleteSubscriber(s)

	if ws.OnNewSubscriber != nil {
		ws.OnNewSubscriber(s)
	}

	for {
		select {
		case msg := <-s.msgs:
			err := writeTimeout(ctx, time.Second*5, c, msg)
			if err != nil {
				return err
			}
		case <-ctx.Done():
			return ctx.Err()
		}
	}
}

// PublishJSON published as json, in calls json.Marshal internally
func (ws *WsHandler) PublishJSON(v interface{}) {
	if msg, err := json.Marshal(v); err == nil {
		ws.Publish(msg)
	}
}

// Publish publishes the msg to all subscribers.
// It never blocks and so messages to slow subscribers
// are dropped.
func (ws *WsHandler) Publish(msg []byte) {
	ws.subscribersMu.Lock()
	defer ws.subscribersMu.Unlock()

	for s := range ws.subscribers {
		ws.PublishTo(s, msg)
	}
}

// PublishJSON published to a single subscriber as json, in calls json.Marshal internally
func (ws *WsHandler) PublishToJSON(s *Subscriber, v interface{}) {
	if msg, err := json.Marshal(v); err == nil {
		ws.PublishTo(s, msg)
	}
}

// PublishTo publishes the msg to a single subscribers.
func (ws *WsHandler) PublishTo(s *Subscriber, msg []byte) {
	select {
	case s.msgs <- msg:
	default:
		go s.closeSlow()
	}
}

// addSubscriber registers a subscriber.
func (ws *WsHandler) addSubscriber(s *Subscriber) {
	ws.subscribersMu.Lock()
	ws.subscribers[s] = struct{}{}
	ws.subscribersMu.Unlock()
}

// deleteSubscriber deletes the given subscriber.
func (ws *WsHandler) deleteSubscriber(s *Subscriber) {
	ws.subscribersMu.Lock()
	delete(ws.subscribers, s)
	ws.subscribersMu.Unlock()
}

// HasSubscriber returns true if there a webclients subscribed, false otherwise
func (ws *WsHandler) HasSubscriber() bool {
	return len(ws.subscribers) > 0
}

func writeTimeout(ctx context.Context, timeout time.Duration, c *websocket.Conn, msg []byte) error {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	return c.Write(ctx, websocket.MessageText, msg)
}
