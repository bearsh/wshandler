[![GoDoc][docimg]][docurl]

[docimg]:      https://pkg.go.dev/badge/gitlab.com/bearsh/wshandler
[docurl]:      https://pkg.go.dev/gitlab.com/bearsh/wshandler

# Simple Websocket Handler

Publish messages to all connected clients

```go
wsh := wshandler.NewWsHandler(func(s *wshandler.Subscriber) {
	// on new connection
}, true)
http.HandleFunc("/ws", wsh.SubscribeHandler)
go http.ListenAndServe(":8000", nil)
```
